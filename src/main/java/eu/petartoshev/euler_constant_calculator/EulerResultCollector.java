package eu.petartoshev.euler_constant_calculator;

import java.math.BigDecimal;

public class EulerResultCollector
{

	private BigDecimal result = BigDecimal.ONE;

	public synchronized void add(final BigDecimal calculatorResult)
	{
		result = result.add(calculatorResult);
	}

	public BigDecimal getResult()
	{
		return result;
	}
}
