package eu.petartoshev.euler_constant_calculator;

public class IntervalExtractor
{

	private static final Interval EMPTY = new Interval(0, 0, true);

	private final Config config;

	private boolean hasMore = true;
	private long lastGiven = 1;

	public IntervalExtractor(Config config)
	{
		this.config = config;
	}

	public synchronized Interval getInterval()
	{
		if(!hasMore) {
			return EMPTY;
		}

		final boolean canAddInterval = lastGiven <= config.getIntervalLastElement() - config.getIntervalSize();
		final long intervalEnd = canAddInterval ? lastGiven + config.getIntervalSize() : config.getIntervalLastElement();
		final boolean isEmpty = lastGiven >= config.getIntervalLastElement();
		final Interval interval = new Interval(lastGiven, intervalEnd, isEmpty);

		hasMore = hasMore && canAddInterval;
		lastGiven = interval.end;

		return interval;
	}

}
