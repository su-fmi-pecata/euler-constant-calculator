package eu.petartoshev.euler_constant_calculator;

import java.util.Objects;

public class Interval
{

	public final long start;
	public final long end;
	public final boolean isEmpty;

	public Interval(long start,
					long end,
					boolean isEmpty)
	{
		this.start = start;
		this.end = end;
		this.isEmpty = isEmpty;
	}

	@Override public String toString()
	{
		return "Interval{" +
			   "start=" + start +
			   ", end=" + end +
			   ", isEmpty=" + isEmpty +
			   '}';
	}

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
		{
			return true;
		}
		if (o == null || getClass() != o.getClass())
		{
			return false;
		}
		final Interval interval = (Interval) o;
		return start == interval.start && end == interval.end && isEmpty == interval.isEmpty;
	}

	@Override
	public int hashCode()
	{
		return Objects.hash(start, end, isEmpty);
	}
}
