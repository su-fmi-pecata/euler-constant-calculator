package eu.petartoshev.euler_constant_calculator;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class FactorialCacheMaster
{

	private final Config config;
	private final Printer printer;
	private final Map<Long, BigInteger> cachedValues;

	public FactorialCacheMaster(final Config config,
								final Printer printer)
	{
		this.config = config;
		this.printer = printer;
		cachedValues = new HashMap<>();
		cachedValues.put(0L, BigInteger.ONE);
		cachedValues.put(1L, BigInteger.ONE);
	}

	public void init()
			throws InterruptedException
	{
		final IntervalExtractor intervalExtractor = new IntervalExtractor(config);

		final int threadsCount = getThreadsCount();
		final FactorialCacheWorker[] workers = new FactorialCacheWorker[threadsCount];

		for (int i = 0; i < threadsCount; i++)
		{
			workers[i] = new FactorialCacheWorker(i, printer, intervalExtractor);
			workers[i].start();
		}

		final Map<Interval, BigInteger> allResults = new HashMap<>();
		for (final FactorialCacheWorker worker : workers)
		{
			worker.join();
			allResults.putAll(worker.getResults());
		}
		combineResults(allResults);
	}

	private int getThreadsCount()
	{
		if (config.getFactorialThreads() < 1)
		{
			return config.getNumberOfThreads();
		}
		return config.getFactorialThreads();
	}

	public BigInteger getFactorial(final long n)
	{
		return cachedValues.get(n);
	}

	private void combineResults(final Map<Interval, BigInteger> allResults)
	{
		final IntervalExtractor intervalExtractor = new IntervalExtractor(config);
		for(Interval interval = intervalExtractor.getInterval(); !interval.isEmpty; interval = intervalExtractor.getInterval())
		{
			final BigInteger current = cachedValues.get(interval.start - 1);
			final BigInteger intervalValue = allResults.get(interval);
			final BigInteger endResult = current.multiply(intervalValue);
			cachedValues.put(interval.end - 1, endResult);
		}

	}
}
