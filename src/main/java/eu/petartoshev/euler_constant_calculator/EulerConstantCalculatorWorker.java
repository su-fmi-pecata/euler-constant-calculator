package eu.petartoshev.euler_constant_calculator;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class EulerConstantCalculatorWorker
		extends Thread
{

	private final int threadId;
	private final int precision;
	private final String logPrefix;
	private final Printer printer;
	private final FactorialCacheMaster factorialCacheMaster;
	private final IntervalExtractor intervalExtractor;
	private final EulerResultCollector eulerResultCollector;

	public EulerConstantCalculatorWorker(int threadId,
										 int precision,
										 Printer printer,
										 FactorialCacheMaster factorialCacheMaster,
										 IntervalExtractor intervalExtractor,
										 EulerResultCollector eulerResultCollector)
	{
		this.threadId = threadId;
		this.logPrefix = String.format("Thread[%04d]: ", threadId);
		this.precision = precision;
		this.printer = printer;
		this.factorialCacheMaster = factorialCacheMaster;
		this.intervalExtractor = intervalExtractor;
		this.eulerResultCollector = eulerResultCollector;
	}

	@Override
	public void run()
	{
		while (true)
		{
			final Interval interval = intervalExtractor.getInterval();
			print(false, interval);
			if (interval.isEmpty)
			{
				print(false, "Pool is empty. Finishing...");
				return;
			}

			calculateInterval(interval.start, interval.end);
		}
	}

	private void print(final boolean required,
					   final Object object)
	{
		printer.print(required, logPrefix + object);
	}

	private void calculateInterval(final long start,
								   final long end)
	{
		BigInteger factorial = factorialCacheMaster.getFactorial(start - 1);
		for (long i = start; i < end; i++)
		{
			factorial = factorial.multiply(BigInteger.valueOf(i));
			final BigDecimal result = BigDecimal.ONE.divide(new BigDecimal(factorial), precision, RoundingMode.CEILING);
			eulerResultCollector.add(result);
		}
	}
}
