package eu.petartoshev.euler_constant_calculator;

import java.time.OffsetDateTime;

public class Printer
{

	private final Config config;

	public Printer(Config config)
	{
		this.config = config;
	}

	public void print(final boolean required,
					  final String message)
	{
		if (!required && !config.isVerbose())
		{
			return;
		}

		System.out.println(OffsetDateTime.now() + " [INFO] " + message);
	}
}
