package eu.petartoshev.euler_constant_calculator;

public class EulerConstantCalculatorMaster
{

	private final Config config;
	private final IntervalExtractor intervalExtractor;
	private final FactorialCacheMaster factorialCacheMaster;
	private final EulerResultCollector eulerResultCollector;
	private final Printer printer;

	public EulerConstantCalculatorMaster(final Config config,
										 final FactorialCacheMaster factorialCacheMaster,
										 final Printer printer)
	{
		this.config = config;
		this.intervalExtractor = new IntervalExtractor(config);
		this.factorialCacheMaster = factorialCacheMaster;
		this.eulerResultCollector = new EulerResultCollector();
		this.printer = printer;
	}

	public void start()
			throws InterruptedException
	{
		printer.print(false, config.toString());

		final int threads = config.getNumberOfThreads();
		final EulerConstantCalculatorWorker[] workers = new EulerConstantCalculatorWorker[threads];

		for (int threadId = 0; threadId < threads; threadId++)
		{
			printer.print(false, "Starting thread " + threadId);
			workers[threadId] = new EulerConstantCalculatorWorker(threadId,
																  config.getPrecision(),
																  printer,
																  factorialCacheMaster,
																  intervalExtractor,
																  eulerResultCollector);
			workers[threadId].start();
		}

		printer.print(false, "Waiting for threads to finish");
		for (int threadId = 0; threadId < threads; threadId++)
		{
			workers[threadId].join();
		}

		printer.print(true, eulerResultCollector.getResult().toPlainString());
	}
}
