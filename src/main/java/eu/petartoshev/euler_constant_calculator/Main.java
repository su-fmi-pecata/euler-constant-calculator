package eu.petartoshev.euler_constant_calculator;

import picocli.CommandLine;

public class Main
{

	public static void main(String[] args)
			throws InterruptedException
	{
		final Config config = getConfig(args);
		final Printer printer = getPrinter(config);
		final FactorialCacheMaster factorialCacheMaster = getFactorialCache(config, printer);
		final EulerConstantCalculatorMaster coordinator = new EulerConstantCalculatorMaster(config, factorialCacheMaster, printer);
		coordinator.start();
	}

	private static Printer getPrinter(final Config config)
	{
		return new Printer(config);
	}

	private static FactorialCacheMaster getFactorialCache(final Config config,
														  final Printer printer)
			throws InterruptedException
	{
		final FactorialCacheMaster factorialCacheMaster = new FactorialCacheMaster(config, printer);
		factorialCacheMaster.init();
		return factorialCacheMaster;
	}

	private static Config getConfig(final String[] args)
	{
		final Config config = new Config();
		new CommandLine(config).parseArgs(args);
		validateConfig(config);
		return config;
	}

	private static void validateConfig(final Config config)
	{
		if (config.getNumberOfThreads() <= 0)
		{
			throw new RuntimeException("Number of threads must be positive");
		}
	}
}
