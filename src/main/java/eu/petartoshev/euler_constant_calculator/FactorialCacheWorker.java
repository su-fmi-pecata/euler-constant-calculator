package eu.petartoshev.euler_constant_calculator;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.LongStream;

public class FactorialCacheWorker
		extends Thread
{

	final IntervalExtractor intervalExtractor;
	private final int threadId;
	private final String logPrefix;
	private final Printer printer;
	private final Map<Interval, BigInteger> resultCollector;

	public FactorialCacheWorker(final int threadId,
								final Printer printer,
								final IntervalExtractor intervalExtractor)
	{
		this.threadId = threadId;
		this.logPrefix = String.format("FactorialThread[%04d]: ", threadId);
		this.printer = printer;
		this.intervalExtractor = intervalExtractor;
		this.resultCollector = new HashMap<>();
	}

	private static BigInteger calculateInterval(final long start,
												final long end)
	{
		return LongStream.range(start, end)
						 .boxed()
						 .map(BigInteger::valueOf)
						 .reduce(BigInteger.ONE, BigInteger::multiply);
	}

	@Override
	public void run()
	{
		while (true)
		{
			final Interval interval = intervalExtractor.getInterval();
			print(false, interval);
			if (interval.isEmpty)
			{
				print(false, "Pool is empty. Finishing...");
				return;
			}

			final BigInteger result = calculateInterval(interval.start, interval.end);
			resultCollector.put(interval, result);
		}
	}

	public Map<Interval, BigInteger> getResults()
	{
		return Map.copyOf(resultCollector);
	}

	private void print(final boolean required,
					   final Object object)
	{
		printer.print(required, logPrefix + object);
	}
}
