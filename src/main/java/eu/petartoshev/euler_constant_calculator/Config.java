package eu.petartoshev.euler_constant_calculator;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(version = "1.0.0")
public class Config
{

	@Option(names = {"-p", "--precision"}, defaultValue = "10")
	private int precision;

	@Option(names = {"-t", "--numberOfThreads", "--threads", "--threadsNumber"}, defaultValue = "4")
	private int numberOfThreads;

	@Option(names = {"-v", "--verbose"}, defaultValue = "false")
	private boolean verbose;

	@Option(names = {"--intervalSize"})
	private int intervalSize = 10 * 1000;

	@Option(names = {"--lastElementOfIntervals"})
	private long intervalLastElement = Long.MAX_VALUE;

	@Option(names = {"--factorialThreads"}, defaultValue = "-1")
	private int factorialThreads;

	public int getPrecision()
	{
		return precision;
	}

	public int getNumberOfThreads()
	{
		return numberOfThreads;
	}

	public boolean isVerbose()
	{
		return verbose;
	}

	public int getIntervalSize()
	{
		return intervalSize;
	}

	public long getIntervalLastElement()
	{
		return intervalLastElement;
	}

	public int getFactorialThreads()
	{
		return factorialThreads;
	}

	@Override
	public String toString()
	{
		return "Config{" +
			   "precision=" + precision +
			   ", numberOfThreads=" + numberOfThreads +
			   ", verbose=" + verbose +
			   '}';
	}
}
